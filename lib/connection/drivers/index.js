module.exports = (function () {

  return {
    MySQL: require('./mysql'),
    Postgres: require('./postgres')
  }

}());
