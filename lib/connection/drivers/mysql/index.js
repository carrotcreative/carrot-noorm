var Promise = require('bluebird');

module.exports = (function () {

  var MySQL = function (config) {
    var lib = require('mysql')
      , config = configFormat(config)
      , connection = lib.createConnection(config);
    this.connect = function () {
      var self = this;
      return new Promise(
        function (resolve, reject) {
          return connection.connect(
            function (error) {
              if (error) return errorHandler(error, reject);
              return resolve(self);
            }
          )
        }
      );
    }
    this.disconnect = function () {
      var self = this;
      return new Promise(
        function (resolve, reject) {
          return connection.end(
            function (error) {
              if (error) return errorHandler(error, reject);
              return resolve(self);
            }
          )
        }
      );
    }
    this.query = function (query) {
      return new Promise(
        function (resolve, reject) {
          return connection.query(query,
            function (error, result) {
              if (error) return errorHandler(error, reject);
              return resolve(result);
            }
          )
        }
      );
    }
  }

  function configFormat (config) {
    var conf = {};
    conf.port = (config.port || 3306);
    conf.host = config.host;
    conf.user = config.user;
    conf.password = config.password;
    conf.database = config.database;
    conf.timezone = config.timezone;
    return conf;
  }

  function errorHandler (error, reject) {
    // TODO: better
    return reject(error);
  }

  return MySQL;

}());
