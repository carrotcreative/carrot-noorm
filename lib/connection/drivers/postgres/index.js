var Promise = require('bluebird');

module.exports = (function () {

  var Postgres = function (config) {
    var lib = require('postgres')
      , config = configFormat(config)
      , connection = lib.Client(config);

    this.connect = function () {
      var self = this;
      return new Promise(
        function (resolve, reject) {
          return connection.connect(
            function (error) {
              if (error) return errorHandler(error, reject);
              return resolve(self);
            }
          )
        }
      );
    }
    this.disconnect = function () {
      var self = this;
      return new Promise(
        function (resolve, reject) {
          return resolve(connection.end());
        }
      );
    }
    this.query = function (query) {
      return new Promise(
        function (resolve, reject) {
          return connection.query(query,
            function (error, result) {
              if (error) return errorHandler(error, reject);
              return resolve(result);
            }
          )
        }
      );
    }
  }

  function configFormat (config) {
    // TODO: manipulate.
    return config;
  }

  function errorHandler (error, reject) {
    // TODO: better
    return reject(error);
  }

  return Postgres;

}());
