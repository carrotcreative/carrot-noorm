var Drivers = require('./drivers');

module.exports = (function () {

  var Connection = function (config) {
    this.client = config.client;
    this.config = (config.connection || {});
    this.connected = false;
    this.driver = (function (client, config) {
      switch (client) {
        case 'mysql':
          return new Drivers.MySQL(config)
        case 'postgres':
          return new Drivers.Postgres(config);
      }
    }(this.client, this.config));
  }

  Connection.prototype = {

    connect: function () {
      return this.driver.connect();
    },

    disconnect: function () {
      return this.driver.disconnect();
    },

    query: function (query) {
      return this.driver.query(query);
    }

  }

  return Connection;

}());
