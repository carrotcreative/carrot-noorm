var _ = require('lodash');

module.exports = (function () {

  return {

    findBuilder: function (model, options) {
      return function (builder) {
        var select = builder.select().from(model.tableName)
          , values, value;
        for (var key in options) {
          values = options[key];
          if (!_.isArray(values)) values = [values];
          switch (key) {
            case 'where':
              select.where.apply(select, values);
              break;
            case 'innerJoin':
              select.innerJoin.apply(select, values);
              break;
            case 'leftJoin':
              select.leftJoin.apply(select, values);
              break;
            case 'rightJoin':
              select.rightJoin.apply(select, values);
              break;
            case 'limit':
              select.limit.apply(select, values);
              break;
            case 'offset':
              select.offset.apply(select, values);
              break;
          }
        }
        return select;
      }
    },

    resetInstance: function (instance, values) {
      if (_.isObject(values)) instance.values = _.merge(instance.values, values);
      var idAttribute = instance.model.idAttribute
        , idValue = instance.values[idAttribute];
      if (idValue) instance.idValue = idValue;
      instance.changed = {};
      return instance;
    }

  }

}());
