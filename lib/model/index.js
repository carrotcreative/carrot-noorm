var Instance = require('./instance')
  , Relations = require('./relations')
  , helpers = require('./helpers')
  , inflect = require('inflect')
  , _ = require('lodash');

module.exports = (function () {

  var Model = function (noorm, name, options) {
    options = (options || {});
    this.noorm = noorm;
    this.name = name;
    this.tableName = (options.tableName || inflect.pluralize(name).toLowerCase());
    this.idAttribute = (options.idAttribute || 'id');
    this.scopes = (options.scopes || {});
    this.initialize = options.initialize;
    this.timestamps = (function () {
      if (_.isUndefined(options.timestamps)) {
        return {
          createdAt: 'createdAt',
          updatedAt: 'updatedAt'
        }
      } else if (options.timestamps === false) {
        return {
          createdAt: false,
          updatedAt: false
        }
      }
      return options.timestamps;
    }());

    if (options.relations) this.relations = new Relations(this, options.relations);
  }

  Model.prototype = {

    find: function (idValue, options) {
      var idAttribute = this.idAttribute
        , where;
      (where = {})[idAttribute] = idValue;
      return this.findOne(_.extend((options || {}), {where: where}));
    },

    findOne: function (options) {
      return this.findAll(_.extend((options || {}), {limit: 1}))
        .then(
          function (instances) {
            if (instances.length) return instances[0];
          }
        )
    },

    findAll: function (options) {
      var self = this;
      return this.query(helpers.findBuilder(this, options))
        .then(
          function (rows) {
            return _.map(rows,
              function (row) {
                return self.build(row);
              }
            );
          }
        )
        .then (
          function (instances) {
            if (options.include) {
              return self.relations.fetch(instances, options.include)
                .then(
                  function () {
                    return instances;
                  }
                )
            }
            return instances;
          }
        );
    },

    findOrInitialize: function (where, options) {
      var self = this;
      return this.findOne(where, options)
        .then(
          function (instance) {
            if (!instance) instance = self.build(where);
            return instance;
          }
        )
    },

    findOrCreate: function (where, options) {
      return this.findOrInitialize(where, options)
        .then(
          function (instance) {
            if (options.attributes) instance.set(attributes);
            return object.save();
          }
        )
    },

    build: function (attributes) {
      return new Instance(this, attributes);
    },

    create: function (attributes) {
      return this.build(attributes)
        .save();
    },

    update: function (set, where) {
      var self = this;
      return this.query(
        function (builder) {
          return builder
            .update()
            .table(self.tableName)
            .set(set)
            .where(where);
        }
      )
    },

    insert: function (values) {
      var self = this;
      return this.query(
        function (builder) {
          return builder
            .insert()
            .into(self.tableName)
            .values(set);
        }
      )
    },

    query: function (builder) {
      var query = builder(this.noorm.queryBuilder);
      return this.noorm.query(query.toSQL());
    }

  }

  return Model;

}());
