var helpers = require('./helpers')
  , _ = require('lodash');

module.exports = (function () {

  var Instance = function (model, attributes) {
    this.model = model;
    this.values = {};
    this.relations = {};
    helpers.resetInstance(this, attributes);
    if (model.initialize) model.initialize.call(this, attributes);
  }

  Instance.prototype = {

    isNew: function () {
      return !(this.idValue);
    },

    get: function (attr) {
      return this.values[attr];
    },

    set: function () {
      var attrs, value;
      if (arguments.length == 2) {
        (attrs = {})[arguments[0]] = arguments[1];
      } else {
        attrs = arguments[0]
      }
      for (var attr in attrs) {
        value = attrs[attr];
        this.values[attr] = value;
        this.changed[attr] = value;
      }
      return this;
    },

    save: function () {
      var self = this
        , where;
      if (isNew) {
        if (this.model.timestamps.createdAt) {
          this.set(this.model.timestamps.createdAt, new Date());
        }
        return this.model.insert(this.values)
          .then(
            function (result) {
              self.idValue = result[self.model.idAttribute];
              return helpers.resetInstance(self, result);
            }
          );
      } else if (_.keys(this.changed).length) {
        if (this.model.timestamps.updatedAt) {
          this.set(this.model.timestamps.updatedAt, new Date());
        }
        (where = {})[this.model.idAttribute] = this.idValue;
        return this.model.update(this.changed, where,
          function (result) {
            return helpers.resetInstance(self, result);
          }
        )
      }
      return this;
    },

    toJSON: function () {
      return this.values;
    }

  }

  return Instance;

}());
