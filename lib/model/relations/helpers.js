var _ = require('lodash');

module.exports = (function () {

  return {
    idValues: function (instances) {
      return _.map(instances,
        function (instance) {
          return instance.idValue;
        }
      )
    }
  }

}());
