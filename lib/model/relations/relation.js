var HasMany = require('./hasMany')
  , BelongsTo = require('./belongsTo')
  , _ = require('lodash');

module.exports = (function () {

  var Relation = function (model, kind, name, options) {
    this.model = model;
    this.kind = kind;
    this.name = name;
    if (_.isObject(options)) {
      this.relatedModelName = options.model;
    } else if (_.isString(options)) {
      this.relatedModelName = options;
      options = {};
    }
    this.adapter = (function (self) {
      switch (kind) {
        case 'hasMany':
          return new HasMany(self, options);
        case 'belongsTo':
          return new BelongsTo(self, options);
      }
    }(this));
  }

  Relation.prototype = {

    relatedModel: function () {
      return this.model.noorm.models[this.relatedModelName];
    },

    fetch: function (idValues) {
      var self = this
        , relatedModel = this.relatedModel()
        , instances;
      return this.model.query(
        function (builder) {
          return self.adapter.query(idValues, builder.select()
            .from(relatedModel.tableName));
        }
      )
      .then(
        function (rows) {
          return _.map(rows,
            function (row) {
              return relatedModel.build(row);
            }
          )
        }
      ).then(
        function (instances) {
          if (self.adapter.isSingle) return instances[0];
          return instances;
        }
      )
    }

  }

  return Relation;

}());
