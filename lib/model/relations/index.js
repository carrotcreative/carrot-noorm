var Relation = require('./relation')
  , helpers = require('./helpers')
  , _ = require('lodash')
  , Promise = require('bluebird');

module.exports = (function () {

  var Relations = function (model, config) {
    this.model = model;
    this.relations = [];
    for (var kind in config) {
      for (var name in config[kind]) {
        this.relations[name] = new Relation(this.model, kind, name, config[kind][name]);
      }
    }
  }

  Relations.prototype = {

    fetch: function (instances, relations) {
      if (!_.isArray(instances)) instances = [instances];
      if (!_.isArray(relations)) relations = [relations];
      var self = this
        , idValues = helpers.idValues(instances)
        , relation;
      return Promise.map(relations,
        function (name, index) {
          relation = self.relations[name];
          return relation.fetch(idValues)
            .then(
              function (related) {
                instances[index].relations[relation.name] = related;
                return instances[index];
              }
            )
        }
      )
    }

  }

  return Relations;

}());
