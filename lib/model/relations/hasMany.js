module.exports = (function () {

  return function (relation, options) {
    var relatedModel = relation.relatedModel()
      , foreignKey = (options.foreignKey || (relatedModel.tableName + '_' + relatedModel.idAttribute));
    this.query = function (idValues, builder) {
      return builder
        .whereIn(foreignKey, idValues);
    }
  }

}());
