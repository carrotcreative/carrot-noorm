var _ = require('lodash');

module.exports = (function () {

  return function (query) {
    this.sets = {};
    this.wheres = [];
    this.table = function (tableName) {
      this.tableName = tableName;
      return this;
    }
    this.set = function (object) {
      var keys = []
        , values = []
        , attributes;
      if (arguments.length == 1) {
        _.extend(this.sets, object);
        return this;
      }
      this.sets[arguments[0]] = arguments[1];
      return this;
    }
    this.where = function () {
      this.wheres = this.wheres.concat(query.helpers.whereClause.apply(null, arguments));
      return this;
    }
    this.toSQL = function () {
      var sets = this.sets
        , sql = ['update', query.helpers.tableFormat(this.tableName), 'set']
        , setters = [];
      for (var attr in sets) {
        setters.push([query.helpers.columnFormat(attr), '=', query.helpers.valueFormat(sets[attr])].join(' '));
      }
      sql.push(setters.join(', '));
      if (this.wheres.length) {
        sql.push('where');
        sql.push(query.helpers.whereFormat(this.wheres));
      }
      return sql.join(' ');
    }
  }

}());
