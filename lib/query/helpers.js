var _ = require('lodash');

module.exports = (function (client) {

  var helpers = {};

  helpers.joinBuilder = function (table) {
    return {
      ons: [],
      on: function () {
        if (!this.ons.length) this.ons.push('on');
        this.ons.push(Array.prototype.slice.call(arguments, 0))
        return this;
      },
      andOn: function () {
        if (!this.ons.length) return this;
        this.ons.push('and');
        return this.on.apply(this, arguments);
      },
      orOn: function () {
        if (!this.ons.length) return this;
        this.ons.push('or');
        return this.on.apply(this, arguments);
      }
    }
  }

  helpers.whereClause = function (attributes) {
    var clause = [];
    if (_.isObject(attributes)) {
      for (var key in attributes) {
        clause.push([key, '=', attributes[key]]);
      }
    } else if (_.isArray(attributes)) {
      for (var i=0; i<attributes.length; i++) {
        clause.push(attributes[i]);
      }
    } else {
      clause.push(Array.prototype.slice.call(arguments, 0));
    }
    return clause;
  }

  helpers.onClause = function () {
    return helpers.clauseArray.apply(null, arguments);
  }

  helpers.onFormat = function (ons, tableName) {
    ons[0] = helpers.columnFormat(ons[0], tableName);
    ons[2] = helpers.columnFormat(ons[2], tableName);
    return ons;
  }

  helpers.tableFormat = function (tableName) {
    return '`' + tableName + '`';
  }

  helpers.columnFormat = function (column, tableName) {
    var match;
    if (match = column.match(/^(`)?([a-zA-Z0-9_-]+)\.([a-zA-Z0-9_-]+)\1$/)) {
      return helpers.tableFormat(match[2]) + '.`' + match[3] + '`';
    } else if (tableName) {
      return helpers.tableFormat(tableName) + '.`' + column + '`';
    }
    return '`' + column + '`';
  }

  helpers.whereFormat = function (wheres, tableName) {
    var arr = []
      , where;
    for (var i=0; i<wheres.length; i++) {
      where = wheres[i];
      where[0] = helpers.columnFormat(where[0], tableName);
      if (where.length == 3) {
        where[2] = helpers.valueFormat(where[2]);
      } else if (_.isArray(where[1])) {
        where[1] = 'in(' + (helpers.mapValues(where[1]).join(',')) + ')';
      }
      arr.push(where.join(' '));
      if (i < (wheres.length - 1)) sql.push('and');
    }
    return arr.join(' ');
  }

  helpers.valueFormat = function (value) {
    if (_.isNumber(value)) return value;
    return '\'' + value + '\''
  }

  helpers.mapValues = function (values) {
    return _.map(values,
      function (value) {
        return helpers.valueFormat(value);
      }
    )
  }

  return helpers;

});
