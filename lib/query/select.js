var _ = require('lodash');

module.exports = (function () {

  return function (query) {
    this.wheres = [];
    this.joins = [];
    this.from = function (tableName) {
      this._from = tableName;
      return this;
    }
    this.where = function (attributes) {
      this.wheres = this.wheres.concat(query.helpers.whereClause.apply(null, arguments));
      return this;
    }
    this.whereIn = function (attribute, value) {
      var attrs;
      if (arguments.length == 2) {
        (attrs = {})[attribute] = value;
      } else {
        attrs = attribute;
      }
      for (var attr in attrs) this.where(attr, attrs[attr]);
      return this;
    }
    this.innerJoin = function (as) {
      return this.join('inner', as, Array.prototype.slice.call(arguments, 1));
    }
    this.leftJoin = function (as) {
      return this.join('left', as, Array.prototype.slice.call(arguments, 1));
    }
    this.rightJoin = function (as) {
      return this.join('right', as, Array.prototype.slice.call(arguments, 1));
    }
    this.join = function (type, as, args) {
      var join = {}, ons, joinBuilder;
      join.type = type;
      join.tableName = args[0];
      if (as) join.as = as;
      if (args.length == 4) {
        ons = [args[1], args[2], args[3]];
      } else if (args[1]) {
        joinBuilder = query.helpers.joinBuilder(args[0]);
        args[1].call(joinBuilder);
        ons = joinBuilder.ons;
      }
      if (ons) join.ons = ons;
      this.joins.push(join);
      return this;
    }
    this.limit = function (num) {
      this._limit = num;
      return this;
    }
    this.offset = function (num) {
      this._offset = num;
      return this;
    }
    this.toSQL = function () {
      var sql = ['select', '*', 'from', query.helpers.tableFormat(this._from)]
        , join;
      for (var x=0; x<this.joins.length; x++) {
        join = this.joins[x];
        sql = sql.concat([join.type, 'join', query.helpers.tableFormat(join.tableName)]);
        if (join.as) sql = sql.concat(['as', query.helpers.columnFormat(join.as)]);
        if (join.ons) {
          for (var y=0; y<join.ons.length; y++) {
            if (_.isString(join.ons[y])) {
              sql.push(join.ons[y]);
            } else if (_.isArray(join.ons[y])) {
              sql.push(query.helpers.columnFormat(join.ons[y][0], this._from));
              sql.push(join.ons[y][1]);
              sql.push(query.helpers.columnFormat(join.ons[y][2], this._from));
            }
          }
        }
      }
      if (this.wheres.length) {
        sql.push('where');
        sql.push(query.helpers.whereFormat(this.wheres, this._from));
      }
      if (this._offset || this._limit) sql.push('limit');
      if (this._offset) {
        if (_.isUndefined(this._limit)) {
          sql.push(this._offset + ',0');
        } else {
          sql.push(this._offset + ',');
        }
      }
      return sql.join(' ');
    }
  }

}());
