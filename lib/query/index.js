var Select = require('./select')
  , Insert = require('./insert')
  , Update = require('./update')
  , helpers = require('./helpers');

module.exports = (function () {

  var QueryBuilder = function (config) {
    this.client = config.client;
    this.helpers = helpers(this.client);
  }

  QueryBuilder.prototype = {
    select: function () {
      return new Select(this);
    },
    insert: function () {
      return new Insert(this);
    },
    update: function () {
      return new Update(this);
    }
  }

  return QueryBuilder;

}());
