var _ = require('lodash');

module.exports = (function () {

  return function (query) {
    this._values = {};
    this.into = function (tableName) {
      this._into = tableName;
      return this;
    }
    this.values = function (object) {
      var keys = []
        , values = []
        , attributes;
      if (arguments.length == 1) {
        _.extend(this._values, object);
        return this;
      }
      this._values[arguments[0]] = arguments[1];
      return this;
    }
    this.toSQL = function () {
      var values = this._values
        , keys = [], vals = [];
      for (var attr in values) {
        keys.push(query.helpers.columnFormat(attr));
        vals.push(query.helpers.valueFormat(values[attr]));
      }
      return ['insert', 'into',
        this._into,
        '(' + keys.join(',') + ')',
        'values',
        '(' + vals.join(',') + ')'
      ].join(' ');
    }
  }

}());
