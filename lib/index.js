var QueryBuilder = require('./query')
  , Connection = require('./connection')
  , Model = require('./model');

module.exports = (function () {

  var Noorm = function (config) {
    this.config = config;
    this.models = {};
    this.queryBuilder = new QueryBuilder(config);
    this.connection = new Connection(config);
    this.connected = false;
  }

  Noorm.prototype = {

    define: function (name, options) {
      return (this.models[name] = new Model(this, name, options));
    },

    connect: function () {
      var self = this;
      return this.connection.connect()
        .then(
          function () {
            self.connected = true;
            return self;
          }
        );
    },

    disconnect: function () {
      var self = this;
      return this.connection.disconnect()
        .then(
          function () {
            self.connected = false;
            return self;
          }
        );
    },

    query: function (query) {
      return this.connection.query(query);
    }

  }

  return Noorm;

}());
