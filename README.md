noorm
=====

A simple object relational mapping library for node.  

### Installation

```sh
$ npm install --save noorm
$ npm install --save mysql # for MySQL
$ npm install --save pg # for Postgres
```

### Usage

```javascript
var Noorm = require('noorm');

var noorm = new Noorm({
  client: 'mysql',
  connection: {
    host: 'localhost',
    user: 'root',
    password: 'foo',
    database: 'noorm_db'
  }
});

var User = noorm.define('User', {

  relations: {
    hasMany: {
      posts: 'Post'
    }
  }

});

var Post = noorm.define('Post', {

  relations: {
    belongsTo: {
      user: 'User'
    }
  }

});

return noorm.connect()
  .then(
    function () {
      return User.findAll({include: 'post'});
    }
  )
  .then(
    function (users) {
      console.log(users);
    }
  )
```
